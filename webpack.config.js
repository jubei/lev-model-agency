const path = require('path');

var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/main.js',
    output: {
      path: path.resolve(__dirname, './public'),
      filename: 'build.js'
    },
    module: {
        rules: [
          {
            test: /\.vue$/,
            loader: 'vue-loader',
          },
          {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/,
          },
          {
            test: /\.(png|jpg|gif|svg)$/,
            loader: 'file-loader',
            options: {
              name:  'assets/images/[name].[ext]?[hash]',
            }
          },
        ]
      },
      plugins: [
        new CopyWebpackPlugin([
          {from: 'src/static/images', to: 'assets/other'},
          {from: 'terms-and-conditions.html', to: 'terms-and-conditions.html'},
          {from: 'src/assets/', to: 'assets'}
       ]),
        new HtmlWebpackPlugin({
          title: 'LEV Model Agency',
          template: 'main.html',
          filename: path.resolve(__dirname, 'public/index.html'),
          favicon: 'favicon.ico'
        })
      ]
}