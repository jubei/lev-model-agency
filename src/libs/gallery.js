"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Image {
}
exports.Image = Image;
class Gallery {
    constructor(element, customImages, customConfig, cbAnimation, cbReady) {
        this._imagesLoaded = 0;
        this._imagesPositioned = 0;
        this._totalImages = 0;
        this.peaks = [];
        this.currentFocus = 9999;
        this.animationCb = function () { };
        this.readyCb = function () { };
        this.prevLeft = 0;
        this.config = {
            galleryHeight: 500,
            horizontalSpacing: 20,
            verticalSpacing: 0,
            currentFocus: 0,
            galleryStart: 0,
            galleryEnd: undefined
        };
        this.domElement = element;
        this.images = customImages;
        this.animationCb = cbAnimation;
        this.readyCb = cbReady;
        this._totalImages = this.images.length;
        for (var key in customConfig) {
            this.config[key] = customConfig[key];
        }
        if (this.config.galleryEnd === 'undefined')
            this.config.galleryEnd = this.images.length - 1;
        this.domElement.style.height = this.config.galleryHeight + 'px';
        this.currentFocus = this.config.currentFocus;
        this.create();
    }
    focus(index) {
        console.log(this.peaks);
        let horizontalWindowCenter = this.domElement.clientWidth / 2;
        this.animationCb(this.domElement.children[0], 0 + horizontalWindowCenter - this.peaks[index]);
        return true;
    }
    scale() {
        let imagesOnScreen = this.images.length;
        Array.from(this.domElement.querySelectorAll('section')).forEach((element, index) => {
            let leftEdge = parseFloat(element.style.left);
            let rightEdge = parseFloat(element.style.left) + parseFloat(element.style.width);
        });
        this.focus(this.currentFocus);
    }
    position(element) {
        var _this = this;
        setTimeout(function observe() {
            if (element.width) {
                element.parentElement.style.width = element.width + 'px';
                element.parentElement.style.left = _this.prevLeft + _this.config.horizontalSpacing + 'px';
                _this.prevLeft = _this.prevLeft + _this.config.horizontalSpacing + parseFloat(element.width);
                _this.peaks.push(_this.prevLeft - parseFloat(element.width) / 2);
                element.style.display = 'none';
                if (++_this._imagesLoaded === _this._totalImages) {
                    console.log('All images loaded');
                    _this.focus(_this.currentFocus);
                    _this.readyCb();
                }
            }
            else {
                console.log('Width not set yet');
                setTimeout(observe, 200);
            }
        }, 0);
    }
    create() {
        let _this = this;
        var wrapper = document.createElement('div');
        wrapper.id = "wrapper";
        wrapper.style.position = "absolute";
        wrapper.style.left = "0px";
        wrapper.style.top = "0px";
        wrapper.style.width = "auto";
        wrapper.style.width = "auto";
        wrapper.style.backgroundColor = "yellowgreen";
        this.domElement.appendChild(wrapper);
        this.images.forEach(function (image, index) {
            let s = document.createElement('section');
            let i = document.createElement('img');
            i.src = image.source;
            i.title = image.title;
            i.style.visibility = "hidden";
            i.height = _this.config.galleryHeight;
            i.style.height = _this.config.galleryHeight + 'px';
            var prevLeft = 0;
            var prevCenter = undefined;
            i.addEventListener('load', element => { _this.position(element.target); });
            s.appendChild(i);
            s.style.backgroundColor = "#f1f1f1";
            s.style.left = _this.config.horizontalSpacing + 'px';
            s.style.top = _this.config.verticalSpacing + 'px';
            s.style.backgroundImage = "url(" + i.src + ")";
            s.style.left = '-9999px';
            s.style.height = _this.config.galleryHeight + 'px';
            s.style.width = _this.config.galleryHeight + 'px';
            wrapper.appendChild(s);
        });
        return false;
    }
    forward() {
        if (this.currentFocus + 1 <= this.config.galleryEnd) {
            return this.focus(++this.currentFocus);
        }
        else {
            return false;
        }
    }
    backward() {
        if (this.currentFocus - 1 >= 0) {
            return this.focus(--this.currentFocus);
        }
        else {
            return false;
        }
    }
}
exports.default = Gallery;
