var $ = require('jquery');
//var ui = require('jquery-ui');
$.fileupload = require('../assets/javascript/jquery.fileupload');

var ImageUploader = (function (container, token, remoteUrl) {
    var showingErrors = false;
    var filesToUpload = [];

    var config = {
        access: {
            token: {
                name: "token",
                id: "token",
                class: ""
            },
            fileRepo: {
                name: "files",
                id: "file_repo",
                class: ""
            },
            thumbnailContainer: {
                id: "thumbnails"
            },
            uploadErrors: {
                id: "upload_errors"
            },
            progressBar: {
                id: "progress_bar_container"
            }
        }
    }

    /* 1. Creating enviroment for uploading images */

    $('<div>', { class: 'fileinput-button' }).append(
    $('<input>', { name: config.access.fileRepo.name + '[]', id: config.access.fileRepo.id, type: "file", accept: "image/*", multiple: "multiple" })
    ).appendTo('#' + container);
    $('<div>', { id: config.access.thumbnailContainer.id }).appendTo('#' + container);
    $('<div>', { id: config.access.uploadErrors.id }).appendTo('#' + container);
    $('<div>', { id: config.access.progressBar.id }).appendTo('#' + container);
    $('<div>', { class: "progress-bar" }).appendTo('#' + config.access.progressBar.id);


    /* 2. Init  file uploading */
    $('#' + config.access.fileRepo.id).parent().fileupload({
        url: remoteUrl,
        dataType: 'json',
        autoupload: true,
        maxChunkSize: 1000000, // 1 MB
        done: function (e, data) {
            $.each(data.result[config.access.fileRepo.name], function (index, file) {;

                filesToUpload.push(file);

                if (file.hasOwnProperty('error')) {
                    $('<div/>').css('background-color', "grey").appendTo('#' + config.access.thumbnailContainer.id);

                    if (!showingErrors) {
                        $('#' + config.access.uploadErrors.id).append('<small class="error">We failed to upload some of your pictures. Please, check format and try again. Limit os photos is 10!</small>');
                        showingErrors = true;

                        setTimeout(function () {
                            $('#' + config.access.uploadErrors.id + ' small').fadeOut();
                            showingErrors = false;
                        }, 10000);
                    }

                } else {
                    /* ON SUCCESS */
                    var newImage = $('<img/>', { src: './assets/images/icon-remove.png', title: 'Remove ' + file.name });

                    var newImageContainer = $('<div/>');
                    newImageContainer.css('background-image', "url(" + file.thumbnailUrl + '&token=' + token  +")");
                    newImageContainer.append(newImage);
                    newImageContainer.data("name", file.name);
                    newImageContainer.attr('class', 'thumb')
                    newImageContainer.appendTo('#' + config.access.thumbnailContainer.id);

                }
            });
        },
        fail: function (e, data) {
            console.log('Upload server error', data);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#' + config.access.progressBar.id + ' .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


    /* DELETION of image before sending */
    $('#' + config.access.thumbnailContainer.id).delegate("img", "click", function () {
        
        for (var i = 0; i < filesToUpload.length; i++) {
            if (filesToUpload[i].name == $(this).data('name')) {

                $.ajax({
                    type: "DELETE",
                    url: filesToUpload[i].deleteUrl + '&token=' + token,
                    data: {},
                    cache: false,
                    success: function (output) {
                        console.log('Server successfully deleted image');
                    }
                });

                filesToUpload = filesToUpload.splice(i, 1);
                break;
            }
        }

        $(this).parent().remove();
        
        return true;
    });



    return {
       filesToUpload: filesToUpload
    }
});

module.exports = ImageUploader;