import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'
import App from './App.vue'

/* Component templates */
import GalleryComponent from './components/gallery.vue'
import AboutComponent from './components/about.vue'
import FormComponent from './components/form.vue'
import FormSuccessComponent from './components/form-success.vue'
import FormFailComponent from './components/form-fail.vue'
import ExploreComponent from './components/explore.vue'
import ProfileComponent from './components/profile.vue'
import ContactComponent from './components/contact.vue'

/* Search Component */
import SearchComponent from './components/search.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', 			component: GalleryComponent },
  { path: '/about', 	component: AboutComponent  }, 
  { path: '/explore', component: ExploreComponent },
  { path: '/explore/:id', component: ProfileComponent },
  { path: '/search/:query', component: SearchComponent },
  { path: '/form', component: FormComponent },
  { path: '/form/success', component: FormSuccessComponent },
  { path: '/form/failed', component: FormFailComponent },
  { path: '/contact', component: ContactComponent },
  { path: '/search', component: SearchComponent }
]

//console.log(NODE_ENV);

const router = new VueRouter({
  routes,
  mode: 'hash'
  //mode: 'history'
})


Vue.use(VeeValidate, { delay: 400, classes: true });
Vue.config.ignoredElements = ['fileuploader'];

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
