## LEV Model Agency

This is a ficticious model agencies website. This was built do demo my developer skills only. If you have any question please contact me on Twitter [@justinasbei](https://twitter.com/justinasbei/)

## Stack

* Webpack 4
* Bootstrap
* Vue
* Vue Components
* Vue Router
* Node 8

## Demo

A working version is LIVE @ [https://jubei.gitlab.io/lev-model-agency/#](https://jubei.gitlab.io/lev-model-agency/#) 

## Contribution

No contribution possible.